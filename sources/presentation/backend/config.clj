(ns presentation.backend.config
  (:refer-clojure :exclude [get])
  (:require [environ.core :as environ]))

;;

  (defn- get-config-value [key default & [cast-fn]]
    (let [cast-fn (or cast-fn identity)]
      (or (some-> environ/env key cast-fn)
        default)))

;; Return configuration

  (defn integer [val]
    (Integer. val))

  (defn get []
    (let [environment     (get-config-value :presentation-env            :development  keyword)
          host            (get-config-value :presentation-host           "0.0.0.0")
          port            (get-config-value :presentation-port           3000          integer)
          ssl-port        (get-config-value :presentation-ssl-port       3443          integer)
          keystore-path   (get-config-value :presentation-ssl-keystore   "certificates/server.keystore")
          truststore-path (get-config-value :presentation-ssl-truststore "certificates/server.keystore")]
      {:environment environment
       :host host
       :port port
       :ssl-port ssl-port
       :keystore-path keystore-path
       :truststore-path truststore-path}))
