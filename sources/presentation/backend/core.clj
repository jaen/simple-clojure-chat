(ns presentation.backend.core
  (:require [schema.core :as s]
            [taoensso.timbre :as log]
            [taoensso.timbre.appenders.core :as appenders]
            ;[com.palletops.log-config.timbre.tools-logging :as timbre-logging]
            [mount.core :as mount]

            [presentation.backend.config :as config]
            [presentation.backend.components.application :as application]))

;; === Global setup ==='

  (log/merge-config!
    {:appenders {:println {:enabled? false}
                 :spit    (appenders/spit-appender {:fname "./logs/timbre.log"})
                 ;:jl      (timbre-logging/make-tools-logging-appender {})
                 }})

  (s/set-fn-validation! true)

;; === Application initialisation ===

  (defn start!
    "Starts the application."
    []
    (let [config (config/get)]
      (mount/start-with-args config
        #'presentation.backend.components.application/application)))

  (defn stop!
    "Stops the application."
    []
    (mount/stop
      #'presentation.backend.components.application/application))