(ns presentation.backend.components.application
  (:require [mount.core :as mount]

            [presentation.backend.components.server]
            [presentation.backend.components.chat-handler]))

(mount/defstate application
  :start (mount/start-with-args
           (mount/args)
           #'presentation.backend.components.server/server
           #'presentation.backend.components.chat-handler/chat-handler)
  :stop  (mount/stop
           #'presentation.backend.components.server/server
           #'presentation.backend.components.chat-handler/chat-handler))