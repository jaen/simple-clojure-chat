(ns presentation.backend.components.chat-handler
  (:require
    [immutant.web.async :as web-async]
    [clojure.core.async :as async :refer [go go-loop <! >!]]
    [taoensso.timbre :as log]
    [mount.core :as mount]
    [schema.core :as s]
    [clj-uuid :as uuid]
    [buddy.hashers :as hashers]
    [buddy.sign.jwt :as jwt]

    [presentation.common.utilities.transit :as transit]
    [presentation.common.schemas.events :as event-schemas]
    [presentation.common.utilities.async :refer [wrap-message]]))

(defonce input-chan
  (async/chan))

(defonce input-per-channel-pub
  (async/pub
    input-chan
    (fn [{:keys [channel]}]
      (:id channel))))

(def non-websocket-request
  {:status  400
   :headers {"content-type" "text/plain"}
   :body    "Expected a websocket request."})

(defn hash-password
  [password]

  (hashers/derive password {:alg :bcrypt+sha512}))

(defn valid-passsword?
  [hash password]

  (hashers/check password hash))

(defn make-user
  [{:keys [login password display-name]}]

  {:id           (uuid/v4)
   :login        login
   :password     (hash-password password)
   :display-name (or display-name login)})

(defonce users
  (atom
    {"jaen" (make-user {:login    "jaen"
                        :password "test"})}))

(def token-secret
  "CorrectHorseBatteryStaple")

(defn make-token
  [user]

  (jwt/sign {:user (dissoc user :password)} token-secret))

(defn valid-token?
  [token]

  (try
    (jwt/unsign token token-secret)

    (catch Exception e
      nil)))

(defn log-in-user!
  [{:keys [login password token] :as credentials}]

  ;(log/debug "Got login request:" login-request)
  ;(when (= message-type :log-in!)
  (cond
    token
    (if-let [{:keys [user]} (valid-token? token)]
      [:log-in-success {:user  (dissoc user :password)
                        :token token}]
      [:log-in-error {:token ["is invalid"]}])

    login
    (if-let [user (get @users login)]
      ;; user exists
      (if (valid-passsword? (:password user) password)
        [:log-in-success {:user  (dissoc user :password)
                          :token (make-token user)}]
        [:log-in-error {:password ["does not match login"]}])

      ;; new-user
      (let [new-user (make-user credentials)]
        (swap! users assoc login new-user)
        [:log-in-success {:user  (dissoc new-user :password)
                          :token (make-token new-user)}])))) ;)

(defonce sub-closers
  (atom {}))

(defn handle-websocket!
  [request websocket-handler]

  (let [connection-id (uuid/v1)
        source        (async/chan 16 (comp
                                       (map transit/from-str)
                                       (map #(update % :metadata assoc :conn-id connection-id))))
        sink          (async/chan 16 (map transit/to-str))
        close-subs!   (fn []
                        (doseq [[_ close!] (get @sub-closers connection-id)]
                          (close!)))
        callbacks     {:on-open    (fn [channel]
                                     (log/debug "New websocket connection.")

                                     (go-loop []
                                       (when-let [response (<! sink)]
                                         (web-async/send! channel response)
                                         (recur))))
                       :on-close   (fn [channel {:keys [code reason]}]
                                     (log/debug "Websocket closed.")

                                     (async/close! source)
                                     (async/close! sink)
                                     (close-subs!))
                       :on-error   (fn [channel throwable]
                                     (log/debug "Websocket encountered error:" throwable)

                                     (async/close! source)
                                     (async/close! sink)
                                     (close-subs!))
                       :on-message (fn [channel message]
                                     (async/put! source message))}]
    (websocket-handler {:sink   sink
                        :source source})
    (web-async/as-channel request callbacks)))

(defmulti handle-message!
  (fn [[event _] _]
    (log/debug "Dispatching" event)
    event))

(defonce channels
  (atom {}))

(defmethod handle-message! :log-in!
  [[_ credentials] {:keys [send!]}]

  (let [response (log-in-user! credentials)]
    (send! response)
    (send! [:channels (into {} (map
                                 (fn [[id channel]]
                                   [id (select-keys channel [:id :name])])
                                 @channels))])))

(defmethod handle-message! :log-out!
  [_ _])

(defn make-channel!
  [{:keys [id name]}]

  (let [sub-chan (async/chan)
        _        (async/sub input-per-channel-pub id sub-chan)
        mult     (async/mult sub-chan)]
    {:id      id
     :name    name
     :listen! (fn [callback]
                (let [chan (async/chan)]
                  (async/tap mult chan)
                  (go-loop []
                    (when-let [message (<! chan)]
                      (callback message)
                      (recur)))

                  #(async/close! chan)))}))

(defmethod handle-message! :join-channel!
  [[_ {:keys [id] :as channel}] {:keys [conn-id send!]}]

  (let [{:keys [listen!] :as channel} (or
                                        (get @channels id)
                                        (make-channel! channel))
        unlisten! (listen! #(send! [:message %]))]
    (swap! channels assoc id channel)
    (swap! sub-closers update conn-id assoc id unlisten!)))

(defmethod handle-message! :part-channel!
  [[_ {:keys [id]}] {:keys [conn-id]}]

  (when-let [close! (get-in @sub-closers [conn-id id])]
    (close!)))

(defmethod handle-message! :say!
  [[_ message] {:keys [conn-id]}]

  (async/put! input-chan message))

(defmethod handle-message! :default
  [message metadata]

  (log/debug "No handler for:" message (dissoc metadata :response-chan :send!)))

(defn- make-send!
  [sink]

  (fn send!
    ([message]
     (send! message {}))

    ([message metadata]
     (async/put! sink (wrap-message message metadata)))))

(defn websocket-handler
  [{:keys [source sink]}]

  (let [send! (make-send! sink)]
    (go-loop []
      (when-let [{:keys [type body metadata] :as message} (<! source)]
        (log/debug "Got message:" message)

        (when (= type :message)
          (log/debug "Handling message:" body)
          (handle-message! body {:send!         send!
                                 :metadata      metadata}))

        (recur))
      ;; disconnected
      )))

(defn handler
  [request]

  (if (:websocket? request)
    (handle-websocket! request websocket-handler)
    non-websocket-request))


(mount/defstate chat-handler
  :start {:chat-handler handler}
  :stop (constantly nil))