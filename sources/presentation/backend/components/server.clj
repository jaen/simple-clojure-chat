(ns presentation.backend.components.server
  (:require [immutant.web :as web]
            [mount.core :as mount]
            [taoensso.timbre :as log]

            [presentation.backend.components.chat-handler :refer [chat-handler]]))

;; === Application initialisation ===

  (defn start!
    "Starts the application."
    [config]
    (let [{:keys [environment]} config
          server-parameters     (select-keys config [:host :port])
          handler               (:chat-handler chat-handler)]

      (log/info "Starting the" environment "application with: " server-parameters)
      (web/run handler server-parameters)))

  (defn stop!
    "Stops the application."
    [server-handle]
    (when server-handle
      (web/stop server-handle)
      nil))

  (mount/defstate server
    :start (start! (mount/args))
    :stop  (stop! server))