(ns presentation.frontend.ui.avatar
  (:require [reagent.core :as reagent]

            [goog.string :as gstring]
            [goog.crypt :as crypt]
            [goog.crypt.Md5 :as Md5]

            [jdenticon]))


(defn- md5-hash [str]
  (let [bytes  (crypt/stringToUtf8ByteArray str)
        hasher (goog.crypt.Md5.)]
    (.update hasher bytes)
    (crypt/byteArrayToHex (.digest hasher))))

(defn avatar
  [{:keys [login] :as user}]

  (let [update-icon (fn [component]
                      (let [node (reagent/dom-node component)]
                        (.update js/jdenticon node (md5-hash login))))]
    (reagent/create-class
      {:component-did-mount  update-icon
       :component-did-update update-icon
       :render               (fn []
                               [:canvas.user-avatar {:height 50
                                                     :width  50
                                                     :style  {:height 50
                                                              :width  50}}])})))
