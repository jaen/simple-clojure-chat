(ns presentation.frontend.ui.sidebar
  (:refer-clojure :exclude [atom])
  (:require [reagent.core :as reagent :refer [atom]]
            [re-frame.core :as re-frame :refer [subscribe dispatch]]

            [presentation.common.utilities.core :refer [zip]]
            [presentation.frontend.application.logic]
            [jdenticon]

            [presentation.frontend.ui.avatar :as avatar]))


(defn channel-list-element
  [{:keys [id name unread-count active?] :as channel}]

  [:li.channel {:class-name (when active? "active")
                :on-click   #(dispatch [:switch-channel! channel])}
   [:div.name
    name]
   [:div.unread-count
    (when unread-count
      [:span.tag.tag-danger unread-count])]])

(defn new-channel-form
  []

  (let [channel-name (atom "")
        channels     (subscribe [:channels])
        selected-id  (atom nil)
        open?        (atom true)]
    (fn []
      (let [curr-channels @channels]
        [:form
         [:div.dropdown {:class (when (and @open? (seq curr-channels)) "open")}
          [:div.input-group
           [:input.form-control {:placeholder "Join channel"
                                 :value       @channel-name
                                 :on-change   #(do
                                                (reset! channel-name (.. % -target -value))
                                                #_(reset! open? true))}]
           [:span.input-group-btn
            [:button.btn.btn-primary {:type     "submit"
                                      :on-click (fn [e]
                                                  (dispatch [:join-channel!
                                                             (or
                                                               (some
                                                                 (fn [[_ {:keys [name] :as channel}]]
                                                                   (when (= name @channel-name)
                                                                     channel))
                                                                 @channels)
                                                               {:name @channel-name})])
                                                  (reset! channel-name nil)
                                                  (.preventDefault e))}
             [:i.fa.fa-plus]]]]
          [:div.dropdown-menu
           (doall
             (for [[idx [_ {:keys [id name] :as channel}]] (zip (range) curr-channels)]
               ^{:key (str "channel-suggestion-" id)}
               [:div.dropdown-item {:class    (when (= idx @selected-id) "active")
                                    :on-click #(do
                                                (reset! channel-name name)
                                                #_(reset! open? false))}
                name]))]]]))))

(defn channel-list
  [{:keys [channels]}]

  [:ul.channels
   (for [channel channels]
     ^{:key (str "channel-" (:id channel))}
     [channel-list-element channel])
   [:li.channel.new
    [:div.name
     [new-channel-form]]]])

(defn sidebar
  [_]

  (let [channels (subscribe [:joined-channels])]
    (fn [{:keys [user]}]
      [:div.sidebar
       (when user
         [:div.current-user
          [:div.avatar [avatar/avatar user]]
          [:div.welcome-text
           [:h4 (str "Hi, " (:display-name user) "!")]]])
       [channel-list {:channels @channels}]
       [:div.log-out
        [:button.btn.btn-danger.btn-block {:on-click #(dispatch [:log-out!])}
         [:i.fa.fa-sign-out]
         "Log out!"]]])))