(ns presentation.frontend.ui.log-in
  (:refer-clojure :exclude [atom])
  (:require [reagent.core :as reagent :refer [atom]]
            [re-frame.core :as re-frame :refer [subscribe dispatch]]

            [presentation.common.utilities.core :refer [zip]]
            [presentation.frontend.application.logic]
            [presentation.frontend.utilities.async :as async]
            [jdenticon]
            [goog.string :as gstring]
            [taoensso.timbre :as log]
            [cljs-time.format :as time-format]
            [markdown.core :refer [md->html]]
            [cuerdas.core :as str]))

(defn- spinner
  [{:keys [colour] :or {colour ""}}]

  [:div.sk-three-bounce {:class-name colour}
   [:div.sk-child.sk-bounce1]
   [:div.sk-child.sk-bounce2]
   [:div.sk-child.sk-bounce3]])

(defn- log-in-form
  []

  (let [pending? (subscribe [:log-in-pending?])
        errors   (subscribe [:log-in-errors])
        data     (atom {:login    ""
                        :password ""})]
    (fn []
      [:form
       [:h1.display-4 "BasedChat"]
       [:div.form-group {:class (when (:login @errors) "has-danger")}
        [:input.form-control {:placeholder "Login"
                              :value       (:login @data)
                              :disabled    @pending?
                              :on-change   #(swap! data update :login (constantly (.. % -target -value)))}]]
       [:div.form-group {:class (when (:password @errors) "has-danger")}
        [:input.form-control {:placeholder "Password"
                              :type        :password
                              :value       (:password @data)
                              :disabled    @pending?
                              :on-change   #(swap! data update :password (constantly (.. % -target -value)))}]]
       (if @pending?
         [:button.btn.btn-primary.btn-block {:type     "submit"
                                             :disabled true}
          [spinner {:colour "white"}]]
         [:button.btn.btn-primary.btn-block {:type     "submit"
                                             :disabled @pending?
                                             :on-click (fn [e]
                                                         (dispatch [:log-in! @data])
                                                         (.preventDefault e))}
          [:i.fa.fa-sign-in]
          "Log In"])])))

(defn log-in-screen
  []

  [:div.log-in-screen
   [:div.log-in-form-container
    [log-in-form]]])
