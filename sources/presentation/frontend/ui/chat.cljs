(ns presentation.frontend.ui.chat
  (:refer-clojure :exclude [atom])
  (:require [reagent.core :as reagent :refer [atom]]
            [re-frame.core :as re-frame :refer [subscribe dispatch]]

            [presentation.common.utilities.core :refer [zip]]
            [jdenticon]
            [goog.string :as gstring]
            [taoensso.timbre :as log]
            [markdown.core :refer [md->html]]

            [presentation.common.utilities.time :as time-utils]

            [presentation.frontend.ui.avatar :as avatar]
            [presentation.frontend.ui.sidebar :as sidebar]))

(defn new-message-form
  [{{:keys [id name] :as channel} :channel}]

  (let [data          (atom "")
        send-message! (fn []
                        (dispatch [:say! channel @data])
                        (reset! data ""))]
    (fn [_]
      [:div.new-message-input
       [:div.input-group
        [:textarea.form-control {:placeholder (str "Message #" name "…")
                                 :value       @data
                                 :on-key-down (fn [e]
                                                (when (and
                                                        (= (.-key e) "Enter")
                                                        (not (.-shiftKey e)))
                                                  (send-message!)
                                                  (.preventDefault e)))
                                 :on-change   #(reset! data (.. % -target -value))}]
        [:span.input-group-btn
         [:button.btn.btn-primary {:on-click (fn [e]
                                               (send-message!)
                                               (.preventDefault e))}
          [:i.fa.fa-reply]]]]])))

;(defn single-message
;  [{:keys [user sent-at text]}]
;
;  [:div.message
;   [:div.avatar
;    [user-avatar user]]
;   [:div.body
;    [:div.message-details
;     [:div.user-login (:login user)]
;     [:div.sent-at (time-format/unparse (time-format/formatters :date-hour-minute) sent-at)]]
;    [:div.content
;     text]]])

;(defn channel-messages
;  [{:keys [channel]}]
;
;  [:div.channel-messages
;   (for [{:keys [id] :as message} @(subscribe [:channel-messages channel])]
;     ^{:key (str "message-" id)}
;     [single-messages messages])])

(defn prepare-message
  [message]

  (-> message
    (md->html)
    ;(str/replace #"\n" "<br />")
    (gstring/unescapeEntities)))

(defn collated-messages
  [messages]

  (let [[{:keys [user sent-at] :as first} & _] messages]
    [:div.message
     [:div.avatar
      [avatar/avatar user]]
     [:div.body
      [:div.message-details
       [:div.user-login (:login user)]
       [:div.sent-at (time-utils/to-str sent-at)]]
      (for [{:keys [id text]} messages]
        ^{:key (str "message-" id)}
        [:div.content {:dangerouslySetInnerHTML {:__html (prepare-message text)}}])]]))

(defn channel-messages
  [{:keys [channel]}]

  [:div.channel-messages
   (for [[{:keys [user id]} & _ :as messages]
         (->>
           @(subscribe [:channel-messages channel])
           (partition-by #(get-in % [:user :id])))]
     ^{:key (str "messages-for" (:login user) "-starting-at" id)}
     [collated-messages messages])])

(defn channel-header
  [{:keys [channel]}]

  [:div.channel-header
   [:div.channel-name
    [:h2 (:name channel)]]
   [:div.channel-options
    [:button.btn.btn-danger {:on-click #(dispatch [:part-channel! channel])}
     "Part"]]])

(defn channel-view
  [{:keys [channel]}]

  (log/debug "Channel-view channel is" channel)
  [:div.channel-view
   [channel-header {:channel channel}]
   [channel-messages {:channel channel}]
   [new-message-form {:channel channel}]])

(defn chat-screen
  []

  (let [current-channel (subscribe [:current-channel])]
    (fn [{:keys [user]}]
      [:div.chat-screen
       [:div.row
        [sidebar/sidebar {:user user}]
        [:div.chat-area
         (if-let [channel @current-channel]
           [channel-view {:channel channel}]
           [:div.channel-view.no-channel-selected
            [:div.message
             "Please join a channel"]])]]])))
