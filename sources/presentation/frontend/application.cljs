(ns presentation.frontend.application
  (:require [re-frame.core :as re-frame :refer [subscribe dispatch]]

            [presentation.frontend.application.logic]
            [presentation.common.utilities.core :refer [zip]]
            [presentation.frontend.ui.log-in :as log-in]
            [presentation.frontend.ui.chat :as chat]))

(defn application-component
  []

  (let [current-user (subscribe [:current-user])]
    (fn []
      [:div.chat-application
       (if-let [user @current-user]
         [chat/chat-screen {:user user}]
         [log-in/log-in-screen])])))