(ns presentation.frontend.application.logic
  (:require-macros [reagent.ratom :refer [reaction]]
                   [schema.core])
  (:require [re-frame.core :refer [register-handler
                                   path
                                   register-sub
                                   after
                                   debug
                                   trim-v
                                   reg-event-fx
                                   reg-event-db
                                   reg-fx
                                   reg-sub
                                   inject-cofx]]
            [schema.core :as s]
            [cljs-uuid-utils.core :as uuid]

            [presentation.frontend.application.schemas :as schemas]
            [presentation.frontend.utilities.async :as async]
            [clojure.set :as set]
            [cljs-time.core :as time]
            [taoensso.timbre :as log]))

;; db-schema

(s/def initial-state :- schemas/ApplicationDb
  "Initial application state."

  {:authentication     {:status :not-logged-in}
   :channels           {}
   :current-channel-id nil
   :joined-channel-ids #{}
   :messages           {}})

;; interceptors

(def check-schema-validator
  "Validate if application db matches the schema after application of effects."

  (after (partial s/validate schemas/ApplicationDb)))

(defn common-interceptors [& [db-path]]
  [check-schema-validator
   (when ^boolean js/goog.DEBUG debug)
   (when db-path (path db-path))
   trim-v])

;; effects

(reg-fx :websocket
  (fn [messages]
    (doseq [[event params & [metadata]] messages]
      (async/send! [event params] metadata))))

;;

(defn gen-id
  []

  (uuid/make-random-uuid))

(reg-event-fx :initialise-db
  [check-schema-validator]

  (fn [{:keys [db]} _]
    {:db initial-state}))

(reg-event-fx :update-db
  [check-schema-validator]

  (fn [{:keys [db]} _]
    {:db (apply dissoc
           (merge initial-state db)                         ;; set new data if not alread in app db
           (set/difference (into #{} (keys db))             ;; remove keys not in initial data anymore
             (into #{} (keys initial-state))))}))

(reg-event-fx :log-in!
  [;(inject-cofx :websocket)
   (common-interceptors :authentication)]

  (fn [{:keys [db]} [log-in-data]]
    {:db        (assoc db :status :pending)
     :websocket [[:log-in! log-in-data]]}))

(reg-event-db :log-in-success
  [(common-interceptors :authentication)]

  (fn [authentication [{:keys [user token]}]]
    (-> authentication
      (dissoc :errors)
      (assoc :user user)
      (assoc :status :logged-in))))

(reg-event-db :log-in-error
  [(common-interceptors :authentication)]

  (fn [authentication [errors]]
    (-> authentication
      (dissoc :user)
      (assoc :errors errors)
      (assoc :status :not-logged-in))))

(reg-event-fx :join-channel!
  [(common-interceptors)]

  (fn [{:keys [db]} [{:keys [id name] :as channel}]]
    (if id
      {:db        (as-> db $
                    (update $ :joined-channel-ids conj id)
                    (if-not (get db :current-channel-id)
                      (assoc $ :current-channel-id id)
                      $))
       :websocket [[:join-channel! channel]]}
      (let [id      (gen-id)
            channel (assoc channel :id id)]
        {:db        (as-> db $
                      (update-in $ [:channels] assoc id channel)
                      (update $ :joined-channel-ids conj id)
                      (if-not (get db :current-channel-id)
                        (assoc $ :current-channel-id id)
                        $))
         :websocket [[:join-channel! channel]]}))))

(reg-event-fx :part-channel!
  [(common-interceptors)]

  (fn [{:keys [db]} [{:keys [id name] :as channel}]]
    {:db        (as-> db $
                  (update $ :joined-channel-ids disj id)
                  (if (=
                        id
                        (get db :current-channel-id))
                    (assoc $ :current-channel-id nil)
                    $))
     :websocket [[:part-channel! channel]]}))

(reg-event-db :receive-channels!
  [(common-interceptors)]

  (fn [db [channels]]
    (assoc db :channels channels)))

(reg-event-fx :log-out!
  [(common-interceptors :authentication)]

  (fn [{:keys [db]} _]
    {:db (-> db
           (dissoc :user :token :errors)
           (assoc :status :not-logged-in))}))

(reg-event-db :switch-channel!
  [(common-interceptors)]

  (fn [db [{:keys [id]}]]
    (assoc db :current-channel-id id)))

(reg-event-fx :say!
  [(common-interceptors)]

  (fn [{:keys [db]} [{:keys [id] :as channel} message-text]]
    (let [now              (time/now)
          channel-messages (get-in db [:messages id] {:messages []})
          user             (get-in db [:authentication :user])
          message          {:id      (gen-id)
                            :sent-at now
                            :user    user
                            :text    message-text}
          new-messages     (as-> channel-messages $
                             (assoc $ :last-message-at now)
                             (if (=
                                   id
                                   (get db :current-channel-id))
                               (assoc $ :last-seen-at now)
                               $)
                             (update $ :messages conj message))
          new-db           (update db :messages assoc id new-messages)]
      {:db        new-db
       :websocket [[:say! {:channel {:id id}
                           :message message}]]})))

(reg-event-db :receive-message!
  [(common-interceptors)]

  (fn [db [{:keys [id] :as channel} message]]
    (let [current-user (get-in db [:authentication :user])
          message-user (:user message)]
      (if (not=
              (:id current-user)
              (:id message-user))
        (let [now              (time/now)
              channel-messages (get-in db [:messages id] {:messages []})
              new-messages     (as-> channel-messages $
                                 (assoc $ :last-message-at (get message :sent-at))
                                 (if (=
                                       id
                                       (get db :current-channel-id))
                                   (assoc $ :last-seen-at now)
                                   $)
                                 (update $ :messages conj message))
              new-db           (update db :messages assoc id new-messages)]
          new-db)
        db))))

;; subscriptions

(reg-sub :log-in-pending?
  (fn [db _]
    (= (get-in db [:authentication :status])
      :pending)))

(reg-sub :log-in-errors
  (fn [db _]
    (get-in db [:authentication :errors])))

(reg-sub :current-user
  (fn [db _]
    (get-in db [:authentication :user])))

(reg-sub :current-channel-id
  (fn [db _]
    (db :current-channel-id)))

(reg-sub :current-channel
  :<- [:current-channel-id]
  :<- [:channels]

  (fn [[current-channel-id channels] _]
    (channels current-channel-id)))

(reg-sub :channels
  (fn [db _]
    (:channels db)))

(reg-sub :joined-channel-ids
  (fn [db _]
    (get-in db [:joined-channel-ids])))

(reg-sub :joined-channels
  :<- [:channels]
  :<- [:joined-channel-ids]
  :<- [:current-channel-id]

  (fn [[channels ids current-channel-id] _]
    (map (fn [id]
           (let [channel (channels id)]
             (if (=
                   (:id channel)
                   current-channel-id)
               (assoc channel :active? true)
               channel))) ids)))

(reg-sub :messages
  (fn [db _]
    (:messages db)))

(reg-sub :channel-messages
  :<- [:messages]

  (fn [messages [_ {:keys [id]}]]
    (log/debug "Getting" id "from" messages)
    (get-in messages [id :messages])))

