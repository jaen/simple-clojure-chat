(ns presentation.frontend.application.schemas
  (:require-macros [schema.core :refer [defschema]])
  (:require [schema.core :as s]
            [presentation.common.utilities.schema :refer [DateTime]]))

;; db-schema

  (def ? s/optional-key)

  (defschema User
    {:id           s/Uuid
     :login        s/Str
     :display-name s/Str})

  (defschema Authentication
    {:status                  (s/enum :logged-in :not-logged-in :pending)
     (? :user)   User
     (? :errors) {(? :login)    [s/Str]
                  (? :password) [s/Str]}})

  (defschema Channel
    {:id   s/Uuid
     :name s/Str})

  (defschema Channels
    {s/Uuid Channel})

  (defschema Message
    {:id      s/Uuid
     :user    User
     :sent-at DateTime
     :text    s/Str})

  (defschema ChannelMessages
    {:last-seen-at    DateTime
     :last-message-at DateTime
     :messages        [Message]})

  (defschema ApplicationDb
    {:authentication     Authentication
     :channels           Channels
     :current-channel-id (s/maybe s/Uuid)
     :joined-channel-ids #{s/Uuid}
     :messages           {s/Uuid ChannelMessages}})
