(ns presentation.frontend.utilities.async
  (:require-macros [cljs.core.async.macros :refer [go-loop alt!]])
  (:require [cljs.core.async :as async :refer [put! <! >!]]
            [cljs.core.async.impl.protocols :as p]
            [taoensso.timbre :as log]
            [re-frame.core :as re-frame]

            [presentation.common.utilities.transit :as transit]
            [presentation.common.utilities.async :refer [wrap-message]]))

(def message-queue
  (atom #queue []))

(defonce current-connection
  (atom nil))

(defn make-connection
  [url websocket-handler]

  (let [queued-messages @message-queue
        _               (reset! message-queue #queue [])
        web-socket      (js/WebSocket. url)
        source          (async/chan 32 (map transit/from-str))
        sink            (async/chan 32 (map transit/to-str))
        callbacks       {:on-open    (fn [event]
                                       (log/debug "Connected to" url ".")
                                       (async/onto-chan sink queued-messages false)
                                       (go-loop []
                                         (when-let [message (<! sink)]
                                           (.send web-socket message)
                                           (recur))))
                         :on-close   (fn [event]
                                       (log/debug "Websocket closed.")
                                       (async/close! source)
                                       (async/close! sink))
                         :on-error   (fn [event]
                                       (log/debug "Websocket encountered error:" event)
                                       (async/close! source)
                                       (async/close! sink))
                         :on-message (fn [event]
                                       (async/put! source (.-data event)))}
        send-fn         (fn [message & [metadata]]
                          (async/put! sink (wrap-message message metadata)))
        disconnect-fn   #(do
                          (reset! current-connection nil)
                          (async/close! source)
                          (async/close! sink))]
    (set! (.-onopen web-socket) (:on-open callbacks))
    (set! (.-onerror web-socket) (:on-error callbacks))
    (set! (.-onmessage web-socket) (:on-message callbacks))
    (set! (.-onclose web-socket) (:on-close callbacks))
    (websocket-handler {:sink   sink
                        :source source})
    {:send!       send-fn
     :disconnect! disconnect-fn}))

(defmulti handle-message!
  (fn [[event _] _]
    (log/debug "Dispatching" event)
    event))

(defmethod handle-message! :log-in-success
  [[_ log-in-data] _]

  (re-frame/dispatch [:log-in-success log-in-data]))

(defmethod handle-message! :log-in-error
  [[_ errors] _]

  (re-frame/dispatch [:log-in-error errors]))

(defmethod handle-message! :message
  [[_ {:keys [channel message]}] _]

  (re-frame/dispatch [:receive-message! channel message]))

(defmethod handle-message! :channels
  [[_ new-channels] _]

  (re-frame/dispatch [:receive-channels! new-channels]))

(defmethod handle-message! :default
  [message _]

  (log/debug "No handler for:" message))

(defn websocket-handler
  [{:keys [source sink]}]

  (go-loop []
    (when-let [{:keys [type body metadata] :as message} (<! source)]
      (log/debug "Got message:" message)

      (when (= type :message)
        (log/debug "Handling message:" body)
        (handle-message! body {:response-channel sink
                               :metadata         metadata}))
      (recur))))

(defn connect!
  [url]

  (when-not @current-connection
    (swap! current-connection #(make-connection url websocket-handler)))
  nil)

(defn send!
  [message & [metadata]]

  (if-let [{:keys [send!]} @current-connection]
    (send! message metadata)
    (swap! message-queue conj (wrap-message message metadata))))

(defn disconnect!
  []

  (when-let [{:keys [disconnect!]} @current-connection]
    (reset! current-connection nil)
    ;(reset! current-user nil)
    (disconnect!))
  nil)