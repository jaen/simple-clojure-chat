(ns presentation.frontend.core
  (:require [taoensso.timbre :as log]
            [reagent.core :as reagent]
            [re-frame.core :as re-frame]
            [schema.core :as s]

            [presentation.frontend.application.logic]
            [presentation.frontend.utilities.logging :as logging-utilities]
            [presentation.frontend.application :as application]
            [presentation.frontend.utilities.async :as async]))

(defn mount-root!
  []

  ;(async/disconnect!)
  (re-frame/dispatch-sync [:update-db])
  ;(async/connect! "ws://localhost:3000")
  (reagent/render
    [#'application/application-component]
    (. js/document (querySelector "#application"))))

(defn application-initialise!
  []

  (s/set-fn-validation! true)
  (async/connect! "ws://localhost:3000")
  (when-let [appender (logging-utilities/make-console-appender)]
    (log/merge-config! {:appenders {:console appender}}))
  (log/debug "Starting the application.")
  (re-frame/dispatch-sync [:initialise-db])
  (mount-root!))

(defn application-reload!
  []

  (mount-root!))