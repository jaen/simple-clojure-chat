(ns presentation.common.utilities.schema
  (:require [schema.core :as s #?@(:cljs [:include-macros true])]
            #?@(:cljs [[goog.date]]))
  (:import #?@(:clj [[org.joda.time.DateTime]]
               :cljs [[goog.date.Date]])))

(def DateTime #?(:clj  org.joda.time.DateTime
                 :cljs goog.date.Date))