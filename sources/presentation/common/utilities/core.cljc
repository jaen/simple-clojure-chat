(ns presentation.common.utilities.core)

(defn zip
  "Zips collections."
  [& colls]

  (apply map vector colls))

(def concatv
  "Concat and return a vector."

  (comp vec concat))