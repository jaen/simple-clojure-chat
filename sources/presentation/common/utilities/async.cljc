(ns presentation.common.utilities.async)

(defn wrap-message
  ([message]
   (wrap-message message {}))

  ([message metadata]
   {:type     :message
    :body     message
    :metadata metadata}))