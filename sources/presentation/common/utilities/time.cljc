(ns presentation.common.utilities.time
  (:require #?@(:clj [[clj-time.format :as time-format]
                      [clj-time.coerce :as time-coerce]]
                :cljs [[cljs-time.format :as time-format]
                       [cljs-time.coerce :as time-coerce]])))

;; Generic formatter

(def ^:private app-time-formatter
  "App-wide default date format."

  (time-format/formatters :date-time))

(def ^:private iso-time-formatter
  "Format to ISO string."

  (time-format/formatters :date-time))

(defn to-str
  "Return a string representation of `date-time` according to the `app-time-formatter`."
  [date-time]

  (time-format/unparse app-time-formatter date-time))

(defn from-str
  "Parser `date-time-str` string according to the `app-time-formatter`."
  [date-time-str]

  (time-format/parse app-time-formatter date-time-str))


(defn to-iso
  "Returns time in nanoseconds since UNIX epoch."
  [date-time]

  (time-format/unparse iso-time-formatter date-time))

(defn from-iso
  "Creates a datetime from nanoseconds since UNIX epoch."
  [date-time-str]

  (time-format/parse iso-time-formatter date-time-str))