(ns presentation.common.schemas.events
  (:require [schema.core :as s :refer [defschema]]))

(defschema EventType
  (s/enum :connected :message :disconnected))

(defschema EventBody
  s/Any)

(defschema EventMetadata
  {s/Keyword s/Any})

(defschema Event
  {:type                      EventType
   (s/optional-key :body)     EventBody
   (s/optional-key :metadata) EventMetadata})
