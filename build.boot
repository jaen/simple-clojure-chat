#!/usr/bin/env boot

(set-env!
  :source-paths   #{"development" "sources"}
  :resource-paths #{"resources"})

(require 'build-utilities)

(set-env!
 :dependencies (build-utilities/read-dependencies!)
 :exclusions '[org.clojure/clojure org.clojure/clojurescript]
 :main-class 'presentation.core)

(task-options! aot {:namespace #{(get-env :main-class)}}
               jar {:main (get-env :main-class)}
               pom {:project 'presentation
                    :version "0.1.0"})

(require '[jeluard.boot-notify :as notify]
         '[pandeiro.boot-http :as bh]
         '[adzerk.boot-reload :as br]
         '[adzerk.boot-cljs :as bc]
         '[development-handler]
         '[deraen.boot-sass :as boot-sass :refer [sass]]
         '[powerlaces.boot-cljs-devtools :refer [cljs-devtools]])

; === App tasks

(deftask development
  "Runs development"
  []

  ; (set-env! :source-paths #(conj % "test"))
  (comp
    (watch)
    (build-utilities/update-dependencies)
    (build-utilities/generate-leiningen-project)
    (bh/serve :handler 'development-handler/my-dir-handler :port 8080)
    (br/reload :on-jsload 'presentation.frontend.core/application-reload!)
    (boot-sass/sass)
    (cljs-devtools)
    (bc/cljs :source-map true
             :optimizations :none
             :ids #{"public/assets/javascripts/application"}
             :compiler-options {:parallel-build true
                                :compiler-stats true
                                :devcards true
                                :closure-defines {"goog.DEBUG" true}
                                :foreign-libs [{:file "resources/foreign-libraries/jdenticon/jdenticon.js"
                                                :file-min "resources/foreign-libraries/jdenticon/jdenticon.min.js"
                                                :provides ["jdenticon"]}]})
    (notify/notify)
    (target :dir #{"target"})))

(deftask build
  "Builds an uberjar of this project that can be run with java -jar"
  []

  (comp
    (aot)
    (pom)
    (uber)
    (jar)))

(defn start! []
  (require 'presentation.backend.core)
  (apply (resolve 'presentation.backend.core/start!) []))

(defn stop! []
  (require 'presentation.backend.core)
  (apply (resolve 'presentation.backend.core/stop!) []))
