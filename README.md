# How to run

* install Java,
* install [boot](https://github.com/boot-clj/boot#install),
* run `boot development` from a terminal in the project folder,
* connect to REPL (for eg. `boot repl -c` in a terminal in the project folder) and evaluate `(start!)` to run the server,
* open `localhost:3000`.

# What to try

* edit some code, the application should automagically update in the browser,
* try IntelliJ IDEA with [Cursive plugin](https://cursive-ide.com/userguide/), it's really neat,
* try debugging with [dirac](https://github.com/binaryage/dirac/blob/master/docs/installation.md#setup-dirac-chrome-extension), it lets you evaluate, debug and print ClojureScript in Chrome Devtools as if it were JavaScript.